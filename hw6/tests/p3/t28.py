class blah:
    def __init__(self, thing, others):
        self.thing = thing
        self.others = others
    def getThing(self):
        return self.thing
    def getOthers(self):
        return self.others


a = blah(1, [2,3,4])
print a.getOthers()
print a.getThing()
b = a.getOthers() + [a.getThing()]
